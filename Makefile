
.PHONY: tests

tests:
	python -m unittest discover tests

wheel:
	python setup.py bdist_wheel
