# XMLHashTree

Python library for calculating hash trees (also known as Merkle Tree).

This package contains essentially to important classes:
  * `XMLHashTree`
  * `SaltedXMLHashTree`


## Setup

Currently, this library is not available via PyPI.
You can install it directly from the git repository:
```bash
pip install git+https://gitlab.com/MatthiasLohr/xmlhashtree.git
```


## Example Usage

```python
import xmlhashtree
import xml.etree.ElementTree

tree = xml.etree.ElementTree.parse('input.xml').getroot()
hash_tree = xmlhashtree.XMLHashTree(tree)  # You can also use SaltedXMLHashTree here
```
