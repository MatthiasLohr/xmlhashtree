
from setuptools import setup

setup(
    name='xmlhashtree',
    version='0.1.0',
    author='Matthias Lohr',
    author_email='mail@mlohr.com',
    packages=['xmlhashtree'],
    entry_points={
        'console_scripts': [
            'xmlhashtree=xmlhashtree.__main__:main'
        ]
    }
)
