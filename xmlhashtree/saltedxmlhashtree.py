
import os
from .xmlhashtree import XMLHashTree


class SaltedXMLHashTree(XMLHashTree):
    def __init__(self, root, hash_algorithm='SHA3-256', salt_generator=None, salt_width=32):
        super().__init__(root, hash_algorithm)
        self.salt_generator = salt_generator
        self.salt_width = salt_width
        self.salts = dict()

    def _calculate_node_hash(self, node):
        node_hash = self._create_hash_instance()
        self._update_hash(node, node_hash)
        node_hash.update(self.get_node_salt(node))
        return node_hash.digest()

    def _create_salt(self):
        if self.salt_generator is None:
            return os.urandom(self.salt_width)
        else:
            return self.salt_generator(self.salt_width)

    def get_node_salt(self, node):
        if node not in self.salts:
            self.salts[node] = self._create_salt()
        return self.salts[node]

    def _str_repr_node(self, node, indent=0):
        return self.get_node_salt(node).hex() + ' ' + super(SaltedXMLHashTree, self)._str_repr_node(node, indent)
