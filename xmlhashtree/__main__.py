
import argparse
import xmlhashtree
import xml.etree.ElementTree


def main():
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument('--salted', action='store_true')
    argument_parser.add_argument('file', help='XML File')
    args = argument_parser.parse_args()

    tree = xml.etree.ElementTree.parse(args.file).getroot()
    if args.salted:
        hash_tree = xmlhashtree.SaltedXMLHashTree(tree)
    else:
        hash_tree = xmlhashtree.XMLHashTree(tree)

    print(hash_tree.str_repr())

    return 0
