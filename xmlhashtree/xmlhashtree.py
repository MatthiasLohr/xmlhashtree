
import hashlib
from collections import OrderedDict


class XMLHashTree(object):
    def __init__(self, root, hash_algorithm='SHA3-256'):
        self.root = root
        self.hash_algorithm = hash_algorithm
        self.hashes = dict()

    def get_root_hash(self):
        return self.get_node_hash(self.root)

    def get_node_hash(self, node):
        if node not in self.hashes:
            self.hashes[node] = self._calculate_node_hash(node)
        return self.hashes[node]

    def _calculate_node_hash(self, node):
        node_hash = self._create_hash_instance()
        self._update_hash(node, node_hash)
        return node_hash.digest()

    def _update_hash(self, node, node_hash):
        # add tag name
        node_hash.update(node.tag.encode('utf-8'))
        # add key/value for each attribute
        sorted_attributes = OrderedDict(sorted(node.attrib.items()))
        for key, value in sorted_attributes.items():
            node_hash.update(key.encode('utf-8'))
            node_hash.update(value.encode('utf-8'))
        # add text after tag opening
        if node.text:
            node_hash.update(node.text.encode('utf-8'))
        for child in node:
            # add child node's hash
            node_hash.update(self.get_node_hash(child))
            # add tag between child node's end and next child/end of this node
            if child.tail:
                node_hash.update(child.tail.encode('utf-8'))

    def _create_hash_instance(self):
        return hashlib.new(self.hash_algorithm)

    def str_repr(self):
        return self._str_repr_node(self.root)

    def _str_repr_node(self, node, indent=0):
        indent_str = ' ' * 2 * (indent + 1)
        attribute_str = ''.join(map(lambda x: ' %s="%s"' % (x, node.attrib[x]), node.attrib.keys()))
        result = '%s%s<%s%s>' % (self.get_node_hash(node).hex(), indent_str, node.tag, attribute_str)
        for child in node:
            result += '\n' + self._str_repr_node(child, indent + 1)
        return result

    def __str__(self):
        return '<%s[root=%s]>' % (self.__class__.__name__, self.get_root_hash().hex())
